from keras.models import Sequential as Seq
from keras.layers import *
from keras.datasets import mnist
from keras import backend as K
import keras
from keras.models import model_from_json
import os
import numpy as np
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

num_classes = 10
epochs = 1
img_rows = 28
img_cols = 28
loopepochs = 50
for i in range(0, loopepochs-1):
    print("Epoche ", i)
    (x_train,y_train),(x_test,y_test) = mnist.load_data()
    if K.image_data_format() == 'channels_first':
        x_train = x_train.reshape(x_train.shape[0], 1, img_rows, img_cols)
        x_test = x_test.reshape(x_test.shape[0], 1, img_rows, img_cols)
        input_shape = (1, img_rows, img_cols)
    else:
        x_train = x_train.reshape(x_train.shape[0], img_rows, img_cols, 1)
        x_test = x_test.reshape(x_test.shape[0], img_rows, img_cols, 1)
        input_shape = (img_rows, img_cols, 1)
    x_train = x_train.astype('float32')
    x_test = x_test.astype('float32')
    x_train /= 255
    x_test /= 255
    # convert class vectors to binary class matrices
    y_train = keras.utils.to_categorical(y_train, num_classes)
    y_test = keras.utils.to_categorical(y_test, num_classes)

    # load json and create model
    json_file = open('model.json', 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    loaded_model = model_from_json(loaded_model_json)
    # load weights into new model
    loaded_model.load_weights("model.h5")
    print("Loaded model from disk")

    # evaluate loaded model on test data
    loaded_model.compile(loss=keras.losses.categorical_crossentropy,
                  optimizer=keras.optimizers.Adadelta(),
                  metrics=['accuracy'])
    score = loaded_model.evaluate(x_test, y_test, verbose=0)
    print("%s: %.2f%%" % (loaded_model.metrics_names[1], score[1] * 100))
    loaded_model.fit(x_train, y_train,
              epochs=epochs,
              verbose=1,
              validation_data=(x_test, y_test))
    # serialize model to JSON
    model_json = loaded_model.to_json()
    with open("model.json", "w+") as json_file:
        json_file.write(model_json)
    # serialize weights to HDF5
    loaded_model.save_weights("model.h5")
    print("Saved model to disk")