from keras.models import Sequential as Seq
from keras.layers import *
from keras.datasets import mnist
from keras import backend as K
import keras
import os
import numpy as np

#model = Seq()
#model.add(Dense(32, activation='relu', input_dim=100))
#model.add(Dense(1, activation='sigmoid'))
#model.compile(optimizer='rmsprop',
#              loss='binary_crossentropy',
#              metrics=['accuracy'])

#data = np.random.random((1000, 10))
#labels = np.random.randint(2, size=(1000, 1))

# Train the model, iterating on the data in batches of 32 samples
#model.fit(data, labels, epochs=100, batch_size=32)

#predictionData = np.random.random((12, 100))
#print(model.predict(predictionData))
#print(mnist)

num_classes = 10
epochs = 1
img_rows = 28
img_cols = 28

(x_train,y_train),(x_test,y_test) = mnist.load_data()
#print(x_train.shape[1:])
if K.image_data_format() == 'channels_first':
    x_train = x_train.reshape(x_train.shape[0], 1, img_rows, img_cols)
    x_test = x_test.reshape(x_test.shape[0], 1, img_rows, img_cols)
    input_shape = (1, img_rows, img_cols)
else:
    x_train = x_train.reshape(x_train.shape[0], img_rows, img_cols, 1)
    x_test = x_test.reshape(x_test.shape[0], img_rows, img_cols, 1)
    input_shape = (img_rows, img_cols, 1)
x_train = x_train.astype('float32')
x_test = x_test.astype('float32')
x_train /= 255
x_test /= 255
# convert class vectors to binary class matrices
y_train = keras.utils.to_categorical(y_train, num_classes)
y_test = keras.utils.to_categorical(y_test, num_classes)

inputShape = x_train.shape[1:]

model = Seq()
model.add(Conv2D(32, kernel_size=(3, 3),
                 activation='relu',
                 input_shape=input_shape))
model.add(Conv2D(64, (3, 3), activation='relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.25))
model.add(Flatten())
model.add(Dense(128, activation='relu'))
model.add(Dropout(0.5))
model.add(Dense(num_classes, activation='softmax'))
"""
model.add(Conv1D(32, 3, padding='same', input_shape=inputShape, activation='relu'))
model.add(MaxPooling1D(2))
model.add(Dropout(0.25))
model.add(Flatten())
model.add(Dense(28, activation='relu'))
model.add(Dropout(0.5))
model.add(Dense(num_classes))
model.add(Activation('softmax'))"""
model.compile(loss=keras.losses.categorical_crossentropy,
              optimizer=keras.optimizers.Adadelta(),
              metrics=['accuracy'])
model.fit(x_train, y_train,
          epochs=epochs,
          verbose=1,
          validation_data=(x_test, y_test))
eval = model.evaluate(x_test, y_test, verbose=0)
print('Test loss: ', eval[0])
print('Test accuracy: ', eval[1])
#print((x_test[0].shape))
y_prediction = model.predict(x_test)
print('my prediction is\n', y_prediction[:5])
print('original y is\n', y_test[:5])

# serialize model to JSON
model_json = model.to_json()
with open("model.json", "w+") as json_file:
    json_file.write(model_json)
# serialize weights to HDF5
model.save_weights("model.h5")
print("Saved model to disk")